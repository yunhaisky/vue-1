const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { VueLoaderPlugin } = require('vue-loader')

module.exports = {

    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'ha.js',
    }

    ,
    plugins: [
        new HtmlWebpackPlugin({
            template: './案例.html'
        }

        ),
        new MiniCssExtractPlugin(),
        new VueLoaderPlugin()],
    module: {
        rules: [{
            test: /\.vue$/,
            loader: 'vue-loader'
        },
        {
            test: /\.css$/i,
            use: [MiniCssExtractPlugin.loader, "css-loader"],
        }

            ,
        {
            test: /\.less$/i,
            use: [MiniCssExtractPlugin, "css-loader", "less-loader"],
        },
        {
            test: /\.(png|jpg|gif|jpeg)$/i,
            type: 'asset'
        },
        { // webpack5默认内部不认识这些文件, 所以当做静态资源直接输出即可
            test: /\.(eot|svg|ttf|woff|woff2)$/,
            type: 'asset/resource',
            generator: {
                filename: 'font/[name].[hash:6][ext]'
            }
        },
        {
            test: /\.js$/,
            exclude: /(node_modules|bower_components)/,
            use: {
                loader: 'babel-loader',
                options: {
                    presets: ['@babel/preset-env'] // 预设:转码规则(用bable开发环境本来预设的)
                }
            }
        }
        ],
    },
    devServer: {
        port: 8848, // 端口号
        open: true // 自动打开浏览器
    },



}

    ;