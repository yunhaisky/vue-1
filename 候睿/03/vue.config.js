module.exports = {
    devServer: {
        port: 8848,
        open: true
    },
    lintOnSave: false
}