// 引入jquery
import $ from 'jquery'
$(function() {
    $('#app li:nth-child(odd)').css('color', 'red')
    $('#app li:nth-child(even)').css('color', 'green')
})
import "./css/index.css"
import "./less/index.less"

import imgUrl from './assets/1.gif'
let theImg = document.createElement("img")
theImg.src = imgUrl
document.body.appendChild(theImg)

import "./assets/fonts/iconfont.css"
let theI = document.createElement('i')
theI.className = 'iconfont icon-qq'
document.body.appendChild(theI)

import App from './App.vue'
import Vue from 'vue'

new Vue({
    el: "#app",
    render: (h) => h(App)
})

// 高级语法
const fn = () => {
    console.log("你好babel");
}
console.log(fn) // 这里必须打印不能调用/不使用, 不然webpack会精简成一句打印不要函数了/不会编译未使用的代码
    // 没有babel集成时, 原样直接打包进lib/bundle.js
    // 有babel集成时, 会翻译成普通函数打包进lib/bundle.js