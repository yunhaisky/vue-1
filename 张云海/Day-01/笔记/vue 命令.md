#  Day-01

## 基本使用

[webpack官网](https://webpack.docschina.org/)

### 初始化包环境 

- 初始化包环境   `yarn init`     得到  **package-json** 文件



### 安装依赖包

- 安装依赖包   `yarn add webpack webpack-cli -D`   D:记录在开发环境



### 配置scripts(自定义命令)

配置  **package-json** 文件   

```js
"scripts": {
        "build": "webpack"
    }
```

![image-20210412113028872](assets/image-20210412113028872.png)



### 新建目录src

### 新建src/add/add.js - 定义求和函数导出

```js
export const addFn = (a, b) => a + b
```

### 新建src/index.js导入使用

```js
import {addFn} from './add/add'

console.log(addFn(10, 20));
```

### 运行打包命令

```bash
yarn build
#或者 npm run build
```

> - 总结: src并列处, 生成默认dist目录和打包后默认main.js文件





## webpack 更新打包

> 目标: 以后代码变更, 如何重新打包呢

### 新建src/tool/tool.js - 定义导出数组求和方法

```js
export const getArrSum = arr => arr.reduce((sum, val) => sum += val, 0)
```

### src/index.js - 导入使用

```js
import {addFn} from './add/add'
import {getArrSum} from './tool/tool'

console.log(addFn(10, 20));
console.log(getArrSum([1, 2, 3]));
```

### 重新打包

```bash
yarn build
```

> - 总结1: src下开发环境, dist是打包后, 分别独立
>
> - 总结2: 打包后格式压缩, 变量压缩等



## webpack的配置

目标: 告诉webpack从哪开始打包, 打包后输出到哪里

- 默认入口  `./src/index.js`
- 默认出口 `./dist/main.js`

### 更改

1. 在src并列处新建 `webpack.config.js`(默认)文件

2. 填入配置项

```js
const path = require("path")

module.exports = {
    entry: "./src/main.js", // 入口
    output: { 
        path: path.join(__dirname, "dist"), // 出口路径
        filename: "bundle.js" // 出口文件名
    }
}
```



- 下载jquery模块 `yarn add jquery`



## 插件-自动生成html文件

**插件：更多的功能**

[html-webpack-plugin插件地址](https://www.webpackjs.com/plugins/html-webpack-plugin/)

1. 插件-自动生成html文件 `yarn add html-webpack-plugin  -D`

2. webpack.config.js配置

```js
// 引入自动生成 html 的插件
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
    // ...省略其他代码
    plugins: [
        new HtmlWebpackPlugin({
            template: './public/index.html' // 以此为基准生成打包后html文件
        })
    ]
}
```



## 加载器 - 处理css文件 

- 目标: loaders加载器, 可让webpack处理其他类型的文件, 打包到js中

- 原因: **webpack默认只认识 js 文件和 json文件**

[style-loader文档](https://webpack.docschina.org/loaders/style-loader/)

[css-loader文档](https://webpack.docschina.org/loaders/css-loader/)

1.  安装依赖 `yarn add style-loader css-loader -D`

2. webpack.config.js 配置

```js
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
    // ...其他代码
    module: { 
        rules: [ // loader的规则
          {
            test: /\.css$/i, // 匹配.css结尾的文件
        	use: ["style-loader", "css-loader"], // 让webpack使用者2个loader处理css文件
        	// 从右到左的, 所以不能颠倒顺序
       	 	// css-loader: webpack解析css文件-把css代码一起打包进js中
        	// style-loader: css代码插入到DOM上 (style标签)
          		}
        	]
    	}
	}
```

3. 引入到main.js (因为这里是入口需要产生关系, 才会被webpack找到打包起来)

- `import "./css/index.css"`
- **注意：这里的后缀不能省略**

- 总结: 万物皆模块, 引到入口, 才会被webpack打包, css打包进js中, 然后被嵌入在style标签插入dom上



## 加载器 - 处理less文件

目标: less-loader让webpack处理less文件, less模块翻译less代码

[less-loader文档](https://webpack.docschina.org/loaders/less-loader/)

1. 下载依赖包  `yarn add less less-loader -D`

2. webpack.config.js 配置

```js
module: {
  rules: [ // loader的规则
    // ...省略其他
    {
    	test: /\.less$/,
    	// 使用less-loader, 让webpack处理less文件, 内置还会用less翻译less代码成css内容
        use: [ "style-loader", "css-loader", 'less-loader']
    }
  ]
}
```

3. 引入到main.js中 `import "./less/index.less"`



## 加载器 - 处理图片文件

目标: 用asset module方式(webpack5版本新增)

### webpack5版本

- 如果使用的是webpack5版本的, 直接配置在**webpack.config.js** - 的 rules里即可

```js
{ // 图片文件的配置(仅适用于webpack5版本)
        test: /\.(gif|png|jpg|jpeg)$/,
        type: 'asset' // 匹配上面的文件后, webpack会把他们当做静态资源处理打包
        // 如果你设置的是asset模式
        // 以8KB大小区分图片文件
        // 小于8KB的, 把图片文件转base64, 打包进js中
        // 大于8KB的, 直接把图片文件输出到dist下
      }
```



2. src/assets/准备图文件

3. 在css/less/index.less - 把小图片用做背景图

```less
body{
    background: url(../assets/logo_small.png) no-repeat center;
}
```

4. 在src/main.js - 把大图插入到创建的img标签上, 添加body上显示

```js
// 引入图片-使用
import imgUrl from './assets/1.gif'
const theImg = document.createElement("img")
theImg.src = imgUrl
document.body.appendChild(theImg)
```

5. 打包运行dist/index.html观察2个图片区别

> 总结:  url-loader 把文件转base64 打包进js中, 会有30%的增大, file-loader 把文件直接复制输出



### webpack4及以前

- 如果你用的是webpack4及以前的, 请使用者里的配置

[url-loader文档](https://webpack.docschina.org/loaders/url-loader/)

[file-loader文档](https://webpack.docschina.org/loaders/file-loader/)

1. 下载依赖包  `yarn add url-loader file-loader -D`
2. webpack.config.js 配置

```js
{
  test: /\.(png|jpg|gif|jpeg)$/i,
  use: [
    {
      loader: 'url-loader', // 匹配文件, 尝试转base64字符串打包到js中
      // 配置limit, 超过8k, 不转, file-loader复制, 随机名, 输出文件
      options: {
        limit: 8 * 1024,
      },
    },
  ],
}
```

图片转成 base64 字符串

- 好处就是浏览器不用发请求了，直接可以读取
- 坏处就是如果图片太大，再转`base64`就会让图片的体积增大 30% 左右

3. src/assets/准备图文件

4. 在css/less/index.less - 把小图片用做背景图

```less
body{
    background: url(../assets/logo_small.png) no-repeat center;
}
```

5. 在src/main.js - 把大图插入到创建的img标签上, 添加body上显示

```js
// 引入图片-使用
import imgUrl from './assets/1.gif'
const theImg = document.createElement("img")
theImg.src = imgUrl
document.body.appendChild(theImg)
```

6. 打包运行dist/index.html观察2个图片区别



## 加载器 - 处理字体文件

- 目标: 用asset module技术, asset/resource直接输出到dist目录下

### webpack5

1. 配置webpack.config.js 

```js
{
        test: /\.(eot|svg|ttf|woff|woff2)$/,
        type: 'asset/resource', // 所有的字体图标文件, 都输出到dist下
        generator: { // 生成文件名字 - 定义规则
          filename: 'fonts/[name].[hash:6][ext]' // [ext]会替换成.eot/.woff
        }
      }
```

2. src/assets/ - 放入字体库fonts文件夹
3. 在main.js引入iconfont.css

```js
//  引入字体图标样式文件
import "./assets/fonts/iconfont.css"
let theI = document.createElement("i")
theI.className = "iconfont icon-qq"
document.body.appendChild(theI)
```

4. 执行打包命令-观察打包后网页效果





### webpack4及以前

1. webpack.config.js - 准备配置

   ```js
    { // 处理字体图标的解析
        test: /\.(eot|svg|ttf|woff|woff2)$/,
            use: [
                {
                    loader: 'url-loader',
                    options: {
                        limit: 2 * 1024,
                        // 配置输出的文件名
                        name: '[name].[ext]',
                        // 配置输出的文件目录
                        outputPath: "fonts/"
                    }
                }
            ]
    }
   ```

2. src/assets/ - 放入字体库fonts文件夹

3. 在main.js引入iconfont.css

   ```js
   // 引入字体图标文件
   import './assets/fonts/iconfont.css'
   ```

4. 在public/index.html使用字体图标样式

   ```html
   <i class="iconfont icon-weixin"></i>
   ```

5. 执行打包命令-观察打包后网页效果

> 总结: url-loader和file-loader 可以打包静态资源文件



## 加载器 - 降级处理

目标: 让webpack对高版本 的js代码, 降级处理后打包

写代码演示: 高版本的js代码(箭头函数), 打包后, 直接原封不动打入了js文件中, 遇到一些低版本的浏览器就会报错

原因: **webpack 默认仅内置了 模块化的 兼容性处理**   `import  export`

babel 的介绍 => 用于处理高版本 js语法 的兼容性  [babel官网](https://www.babeljs.cn/)

解决: 让webpack配合babel-loader 对js语法做处理

[babel-loader文档](https://webpack.docschina.org/loaders/babel-loader/)

  1. 安装包

     `yarn add -D babel-loader @babel/core @babel/preset-env`

     

     babel：一个javascript编译器，把高版本js语法降级处理输出兼容的低版本语法

     babel-loader：可以让webpack转译打包的js代码

  2. 配置规则

     ```js
     module: {
       rules: [
          {
             test: /\.m?js$/,
             exclude: /(node_modules|bower_components)/, // 不去匹配这些文件夹下的文件
             use: {
               loader: 'babel-loader', // 使用这个loader处理js文件
               options: { // 加载器选项
                 presets: ['@babel/preset-env'] // 预设: @babel/preset-env 降级规则-按照这里的规则降级我们的js语法
               }
             }
           }
       ]
     }
     ```

3. 在main.js中使用箭头函数(高版本js)

   ```js
   // 高级语法
   const fn = () => {
     console.log("你好babel");
   }
   console.log(fn) // 这里必须打印不能调用/不使用, 不然webpack会精简成一句打印不要函数了/不会编译未使用的代码
   // 没有babel集成时, 原样直接打包进lib/bundle.js
   // 有babel集成时, 会翻译成普通函数打包进lib/bundle.js
   ```

4. 打包后观察lib/bundle.js - 被转成成普通函数使用了 - 这就是babel降级翻译的功能

> 总结: babel-loader 可以让webpack 对高版本js语法做降级处理后打包



## webpack 开发服务器

### 每次修改都需要重新打包

抛出问题: 每次修改代码, 都需要重新 yarn build 打包, 才能看到最新的效果, 实际工作中, 打包 yarn build 非常费时 (30s - 60s) 之间

为什么费时? 

1. 构建依赖
2. 磁盘读取对应的文件到内存, 才能加载  
3. 用对应的 loader 进行处理  
4. 将处理完的内容, 输出到磁盘指定目录  

解决问题: 起一个开发服务器,  在电脑内存中打包, 缓存一些已经打包过的内容, 只重新打包修改的文件, 最终运行加载在内存中给浏览器使用



### 自动刷新

目标: 启动本地服务, 可实时更新修改的代码, 打包**变化代码**到内存中, 然后直接提供端口和网页访问

1. 下载包

   `yarn add webpack-dev-server -D`

2. 配置自定义命令

   ```js
   scripts: {
   	"build": "webpack --config webpack.config.js",
   	"serve": "webpack serve --config webpack.config.js"
   }
   ```

3. 在webpack.config.js中添加服务器配置,**可以指定端口**

   ```js
   module.exports = {
       // ...其他配置
       devServer: {
         port: 3000, // 端口号
         open: true // 自动打开浏览器
       }
   }
   ```

4. 运行命令-启动webpack开发服务器

   ```bash
   yarn serve
   #或者 npm run serve
   ```

> 总结: 以后改了src下的资源代码, 就会直接更新到内存打包, 然后反馈到浏览器上了

**注意：这里是没有 dist 打包文件的，因为这里把项目打包到了服务器的内存里**

![image-20210412194810333](assets/image-20210412194810333.png)





## 面试题

### 1、什么是webpack（必会）

​	webpack是一个打包模块化javascript的工具，在webpack里一切文件皆模块，通过loader转换文件，通过plugin注入钩子，最后输出由多个模块组合成的文件，webpack专注构建模块化项目

### 2、Webpack的优点是什么？（必会）

1. 专注于处理模块化的项目，能做到开箱即用，一步到位
2. 通过plugin扩展，完整好用又不失灵活
3. 通过loaders扩展, 可以让webpack把所有类型的文件都解析打包
4. 区庞大活跃，经常引入紧跟时代发展的新特性，能为大多数场景找到已有的开源扩展

### 3、webpack的构建流程是什么?从读取配置到输出文件这个过程尽量说全（必会）

​    Webpack 的运行流程是一个串行的过程，从启动到结束会依次执行以下流程：

​	1. 初始化参数：从配置文件读取与合并参数，得出最终的参数

 	2. 开始编译：用上一步得到的参数初始化 Compiler 对象，加载所有配置的插件，开始执行编译
 	3. 确定入口：根据配置中的 entry 找出所有的入口文件
 	4. 编译模块：从入口文件出发，调用所有配置的 Loader 对模块进行翻译，再找出该模块依赖的模块，再递归本步骤直到所有入口依赖的文件都经过了本步骤的处理
 	5. 完成模块编译：在经过第4步使用 Loader 翻译完所有模块后，得到了每个模块被翻译后的最终内容以及它们之间的依赖关系
 	6. 输出资源：根据入口和模块之间的依赖关系，组装成一个个包含多个模块的 Chunk，再把每个 Chunk 转换成一个单独的文件加入到输出列表，这步是可以修改输出内容的最后机会
 	7. 输出完成：在确定好输出内容后，根据配置确定输出的路径和文件名，把文件内容写入到文件系统。

在以上过程中，Webpack 会在特定的时间点广播出特定的事件，插件在监听到感兴趣的事件后会执行特定的逻辑，并且插件可以调用 Webpack 提供的 API 改变 Webpack 的运行结果

### 4、说一下 Webpack 的热更新原理(必会)

​	webpack 的热更新又称热替换（Hot Module Replacement），缩写为 HMR。这个机制可以做到不用刷新浏览器而将新变更的模块替换掉旧的模块。

​    HMR的核心就是客户端从服务端拉去更新后的文件，准确的说是 chunk diff (chunk 需要更新的部分)，实际上 WDS 与浏览器之间维护了一个 Websocket，当本地资源发生变化时，WDS 会向浏览器推送更新，并带上构建时的 hash，让客户端与上一次资源进行对比。客户端对比出差异后会向 WDS 发起 Ajax 请求来获取更改内容(文件列表、hash)，这样客户端就可以再借助这些信息继续向 WDS 发起 jsonp 请求获取该chunk的增量更新。

​    后续的部分(拿到增量更新之后如何处理？哪些状态该保留？哪些又需要更新？)由 HotModulePlugin 来完成，提供了相关 API 以供开发者针对自身场景进行处理，像react-hot-loader 和 vue-loader 都是借助这些 API 实现 HMR。

### 5、webpack与grunt、gulp的不同？（必会）

​    **1)** **三者之间的区别**

​       三者都是前端构建工具，grunt和gulp在早期比较流行，现在webpack相对来说比较主流，不过一些轻量化的任务还是会用gulp来处理，比如单独打包CSS文件等。

​       grunt和gulp是基于任务和流（Task、Stream）的。类似jQuery，找到一个（或一类）文件，对其做一系列链式操作，更新流上的数据， 整条链式操作构成了一个任务，多个任务就构成了整个web的构建流程。

​       webpack是基于入口的。webpack会自动地递归解析入口所需要加载的所有资源文件，然后用不同的Loader来处理不同的文件，用Plugin来扩展webpack功能。

​    **2)** **从构建思路来说**

​       gulp和grunt需要开发者将整个前端构建过程拆分成多个`Task`，并合理控制所有`Task`的调用关系 webpack需要开发者找到入口，并需要清楚对于不同的资源应该使用什么Loader做何种解析和加工

​    **3)** **对于知识背景来说**

​       gulp更像后端开发者的思路，需要对于整个流程了如指掌 webpack更倾向于前端开发者的思路

### 6、有哪些常见的Loader？他们是解决什么问题的？（必会）

1、  file-loader：把文件输出到一个文件夹中，在代码中通过相对 URL 去引用输出的文件

2、  url-loader：和 file-loader 类似，但是能在文件很小的情况下以 base64 的方式把文件内容注入到代码中去

3、  source-map-loader：加载额外的 Source Map 文件，以方便断点调试

4、  image-loader：加载并且压缩图片文件

5、  babel-loader：把 ES6 转换成 ES5

6、  css-loader：加载 CSS，支持模块化、压缩、文件导入等特性

7、  style-loader：把 CSS 代码注入到 JavaScript 中，通过 DOM 操作去加载 CSS。

8、  eslint-loader：通过 ESLint 检查 JavaScript 代码

### 7、Loader和Plugin的不同？（必会）

​    **1)** **不同的作用**

​       Loader直译为"加载器"。Webpack将一切文件视为模块，但是webpack原生是只能解析js文件，如果想将其他文件也打包的话，就会用到loader。 所以Loader的作用是让webpack拥有了加载和解析非JavaScript文件的能力。

​    Plugin直译为"插件"。Plugin可以扩展webpack的功能，让webpack具有更多的灵活性。 在 Webpack 运行的生命周期中会广播出许多事件，Plugin 可以监听这些事件，在合适的时机通过 Webpack 提供的 API 改变输出结果。

**2)** **不同的用法**

​    Loader在module.rules中配置，也就是说他作为模块的解析规则而存在。 类型为数组，每一项都是一个Object，里面描述了对于什么类型的文件（test），使用什么加载(loader)和使用的参数（options）

​    Plugin在plugins中单独配置。 类型为数组，每一项是一个plugin的实例，参数都通过构造函数传入。