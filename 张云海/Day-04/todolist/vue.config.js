/* 覆盖webpack的配置 */
module.exports = {
    devServer: { // 自定义服务配置
        open: true,
        port: 3001
    },
    lintOnSave: false // 关闭eslint检查
}