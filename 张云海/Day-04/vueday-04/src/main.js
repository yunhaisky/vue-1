import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false
import Pannel from './components/Pannel_1'
Vue.component("PannelG", Pannel)


new Vue({
    render: h => h(App),
}).$mount('#app')