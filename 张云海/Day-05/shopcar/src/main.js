import Vue from 'vue'
import App from './App.vue'
import "../node_modules/bootstrap/dist/css/bootstrap.min.css"

Vue.config.productionTip = false
    //全局属性 - 一定要在new Vue之上
import axios from 'axios'
//配置基准地址
axios.defaults.baseURL = "https://www.escook.cn"
    //原型上添加$axios的属性名,值就是axios的函数
Vue.prototype.$axios = axios

new Vue({
    render: h => h(App),
}).$mount('#app')