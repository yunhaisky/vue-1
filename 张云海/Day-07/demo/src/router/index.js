// 导入import 遇到文件夹默认找index.js导出来的东西
// 目标: 我们要导出一个路由对象, 给main.js里使用
import Vue from 'vue'
import VueRouter from 'vue-router'
import FindMusic from '../views/FindMusic'
import MyMusic from '../views/MyMusic'
import Ranking from '../views/Child/Ranking'
import Recommend from '../views/Child/Recommend'
import SongList from '../views/Child/SongList'
import Haa from '../views/Child/Sing/Haa'
import Kaa from '../views/Child/Sing/Kaa'
import Yaa from '../views/Child/Sing/Yaa'
Vue.use(VueRouter)
const routes = [{
        path: "/",
        redirect: "/find_music" // 网页刚打开, 默认显示页面
    },
    {
        path: "/find_music",
        component: FindMusic,
        children: [{
                path: "recommend",
                component: Recommend
            },
            {
                path: "ranking",
                component: Ranking
            },
            {
                path: "songList",
                component: SongList,
                children: [{
                        path: "haa",
                        component: Haa
                    },
                    {
                        path: "kaa",
                        component: Kaa
                    },
                    {
                        path: "yaa",
                        component: Yaa
                    },
                ]
            },
        ]
    },
    {
        path: "/my_music",
        component: MyMusic
    },
]

const router = new VueRouter({
    routes
})

router.beforeEach((to, from, next) => {
    let isLogin = false;
    if (to.path === "/my_music" && isLogin === false) {
        alert("请先去登录")
        next(false)
    } else {
        next()
    }
})


export default router