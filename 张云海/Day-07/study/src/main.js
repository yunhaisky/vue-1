import Vue from 'vue'
import App from './App.vue'
import MyHome from './views/01/MyHome'
import MyMovie from './views/01/MyMovie'
import MyAbout from './views/01/MyAbout'
import VueRouter from 'vue-router'
import MyGoods from './views/MyGoods'
import NotFound from './views/NotFound'
Vue.use(VueRouter)
const routes = [{
            path: "/",
            redirect: "/home"
        },
        {
            path: "/home",
            name: "Home",
            component: MyHome
        },
        {
            path: "/movie",
            name: "Movie",
            component: MyMovie
        },
        {
            path: "/about",
            name: "About",
            component: MyAbout
        },
        {
            path: "/goods",
            name: "Goods",
            component: MyGoods
        },
        {
            path: "/goods/:goodsname",
            component: MyGoods
        },
        {
            path: "*",
            component: NotFound
        }
    ]
    // 传入配置对象
    // routes固定配置项-用于设置规则数组
    // value变量名是上面定义的const routes
const router = new VueRouter({
    routes,
    mode: "history"
})
Vue.config.productionTip = false
new Vue({
    // router规定配置项, 给Vue实例注入路由对象
    router,
    render: h => h(App),
}).$mount('#app')