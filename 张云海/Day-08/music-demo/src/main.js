import Vue from 'vue'
import App from './App.vue'

import "@/assets/fonts/iconfont.css" //字体样式文件
import "@/mobile/flexible" //自动适应宽度,改变html的font-size
import "@/styles/reset.css" //初始化标签默认的样式
import router from "@/router" //导入路由对象

//全局注册vant组件 (自动按需引入)
import { NavBar, Tabbar, TabbarItem, Col, Row, Image as VanImage, Cell, CellGroup, Icon, Search, PullRefresh, List } from 'vant';

Vue.use(Tabbar);
Vue.use(TabbarItem);
Vue.use(NavBar);
Vue.use(Col);
Vue.use(Row);
Vue.use(VanImage);
Vue.use(Cell);
Vue.use(CellGroup);
Vue.use(Icon);
Vue.use(Search);
Vue.use(PullRefresh);
Vue.use(List);

Vue.config.productionTip = false

new Vue({
    router,
    render: h => h(App),
}).$mount('#app')